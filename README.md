# blarx

Python implementation of the board game Blokus

Currently supports one human player and up to 3 AI players.

The next step is to teach the AI to play the game.

## Features

- Built with a relative coordinate system