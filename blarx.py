import os.path

import pygame

from game.draw import Draw
from game.menu import Menu
from game.game import Game

pygame.init()
pygame.display.set_caption("Blarx")
pygame.display.set_icon(pygame.image.load("assets/blarx.png"))
print(os.path.exists("assets/blarx.png"))

game = Game()
menu = Menu(game)
draw = Draw(game, menu)

game.initialize_human()

clock = pygame.time.Clock()

menu.active = True

while game.running:
    for event in pygame.event.get():
        menu.handle_event(event)
    draw.draw_game()
    draw.draw_menu()
    draw.flip_and_update()
    clock.tick(60)

pygame.quit()
