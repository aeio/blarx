import pprint

import pygame
from game.settings import settings


class Board:
    rows: int
    columns: int
    block_size = 20
    color: (int, int, int)
    snap_points: list[list[tuple[int, int]]]
    bounds: pygame.rect

    def __init__(self):
        self.block_size = settings.board.block_size
        self.color = tuple(settings.board.board_color)
        self.rows = settings.board.rows
        self.columns = settings.board.columns
        self.origin_point = (
            int(settings.screen.width / 2 - (self.columns / 2) * self.block_size),
            int(settings.screen.height / 2 - (self.rows / 2) * self.block_size),
        )
        self.bounds = pygame.Rect(
            self.origin_point, ((self.columns * self.block_size) + 1, (self.rows * self.block_size) + 1)
        )
        self.place_snap_points()

    def place_snap_points(self):
        self.snap_points = []
        x_start, y_start = self.origin_point

        for x in range(x_start + 1, x_start + int(self.columns * self.block_size), self.block_size):
            row = []
            for y in range(y_start + 1, y_start + int(self.rows * self.block_size), self.block_size):
                row.append((x, y))
            self.snap_points.append(row)

    def get_board_state(self, players):
        # Initialize an empty board state
        board_state = [[0 for _ in range(self.columns)] for _ in range(self.rows)]

        # Iterate over all pieces of all players
        for player in players.get_list():
            for piece in player.pieces:
                if piece.played:  # If the piece is on the board
                    for block in piece.tiles:
                        # Convert the block's position to board coordinates
                        board_x = (block.x - self.origin_point[0]) // self.block_size
                        board_y = (block.y - self.origin_point[1]) // self.block_size

                        # Update the board state
                        board_state[board_y][board_x] = player.number
        pprint.pprint(board_state)
        return board_state
