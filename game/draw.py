import pygame
from PIL import Image, ImageFilter
from pygame import Rect
from pygame.font import Font

from game.menu import Menu, TextButton, GraphicsButton, MenuItem, PlayerName, AIPlayerCount
from game.settings import settings
from game.player import Player
from game.game import Game
from game.piece import Piece


class Draw:
    game: Game
    menu: Menu | None
    screen: pygame.Surface
    board_font: Font
    menu_font: Font
    input_font: Font

    def __init__(self, game: Game, menu: Menu = None):
        self.game = game
        if menu is None:
            menu = Menu(game)
        self.menu = menu
        self.screen = pygame.display.set_mode((settings.screen.width, settings.screen.height), pygame.NOFRAME)
        self.board_font = Font(settings.board.font, 48)
        self.menu_font = Font(settings.menu.font, 70)
        self.input_font = Font(settings.menu.font, 48)

    def draw(self) -> None:
        self.draw_game()

    def draw_game(self) -> None:
        self.screen.fill(self.game.bg_color)
        self.draw_board()
        self.draw_players()

    def blur_game(self) -> None:
        image = pygame.image.tostring(self.screen, "RGB")
        image = Image.frombytes("RGB", self.screen.get_size(), image)
        image = image.filter(ImageFilter.GaussianBlur(5))
        image = image.tobytes()
        image = pygame.image.fromstring(image, self.screen.get_size(), "RGB")
        self.screen.blit(image, (0, 0))

    def draw_menu(self) -> None:
        if self.menu.is_active:
            self.blur_game()
            for item in self.menu.active_menu.items:
                self.draw_menu_item(item)
        else:
            self.draw_menu_item(self.menu.open_menu_button)

    def flip_and_update(self) -> None:
        pygame.display.flip()
        pygame.display.update()

    def draw_players(self) -> None:
        player = self.game.players.get_current()
        for i in range(self.game.players.length):
            self.draw_player_score(player)
            player = player.next
        for i in range(self.game.players.length):
            self.draw_player_pieces(player)
            player = player.next

    def draw_player_score(self, player: Player) -> None:
        name_surface = self.board_font.render(
            f"{player.name} {player.score}",
            True,
            player.color,
            tuple(settings.board.bg_color),
        )
        if player.lost:
            pygame.draw.line(
                name_surface,
                (255, 0, 0),
                (-10, name_surface.get_height() / 2),
                (name_surface.get_width() + 10, name_surface.get_height() / 2),
                5,
            )
        self.screen.blit(name_surface, player.score_position)

    def draw_player_pieces(self, player: Player) -> None:
        for piece in player.pieces:
            self.draw_piece(piece)

    def draw_piece(self, piece: Piece) -> None:
        for block in piece.tiles:
            pygame.draw.rect(
                self.screen, piece.color, pygame.Rect(block.x, block.y, piece.block_size, piece.block_size)
            )
            pygame.draw.rect(
                self.screen,
                (0, 0, 0),
                pygame.Rect(block.x + 2, block.y + 2, piece.block_size - 4, piece.block_size - 4),
            )
            pygame.draw.rect(
                self.screen,
                piece.color,
                pygame.Rect(block.x + 4, block.y + 4, piece.block_size - 8, piece.block_size - 8),
            )

            if settings.debug:
                pygame.draw.circle(self.screen, (255, 0, 0), piece.position, 2, 2)

    def draw_board(self) -> None:
        screen_width = self.screen.get_width()
        screen_height = self.screen.get_height()

        board = self.game.board

        x_start, y_start = board.origin_point

        x_end = x_start + board.columns * board.block_size
        y_end = y_start + board.rows * board.block_size

        for x in range(x_start, x_start + int((board.columns + 1) * board.block_size), board.block_size):
            pygame.draw.line(self.screen, board.color, (x, y_start), (x, y_end), 2)

        for y in range(y_start, y_start + int((board.rows + 1) * board.block_size), board.block_size):
            pygame.draw.line(self.screen, board.color, (x_start, y), (x_end, y), 2)

        pygame.draw.line(
            self.screen,
            board.color,
            (board.block_size, screen_height / 2),
            (x_start - board.block_size, screen_height / 2),
            1,
        )
        pygame.draw.line(
            self.screen,
            board.color,
            (x_end + board.block_size, screen_height / 2),
            (screen_width - board.block_size, screen_height / 2),
            1,
        )
        pygame.draw.line(
            self.screen,
            board.color,
            (screen_width / 2, board.block_size),
            (screen_width / 2, y_start - board.block_size),
            1,
        )
        pygame.draw.line(
            self.screen,
            board.color,
            (screen_width / 2, y_end + board.block_size),
            (screen_width / 2, screen_height - board.block_size),
            1,
        )

        if settings.debug:
            for row in board.snap_points:
                for point in row:
                    pygame.draw.circle(self.screen, (255, 0, 0), point, 1)

            pygame.draw.circle(self.screen, (255, 0, 0), board.origin_point, 1)

    def draw_text_button(self, item: TextButton) -> None:
        txt_surface = self.menu_font.render(item.text, True, item.color)
        offset = (item.rect.width - txt_surface.get_width()) / 2
        self.screen.blit(txt_surface, (item.rect.x + offset, item.rect.y))
        pygame.draw.rect(self.screen, item.color, item.rect, 2)

    def draw_player_name_input(self, item: PlayerName) -> None:
        label_surface = self.input_font.render(item.label, True, item.color)
        text_surface = self.input_font.render(item.text, True, item.color)
        pygame.draw.rect(
            self.screen,
            item.color,
            Rect(
                item.rect.x + label_surface.get_width() + 25,
                item.rect.y - 5,
                item.rect.width - label_surface.get_width() - 25,
                label_surface.get_height() + 5,
            ),
            2,
        )
        self.screen.blit(label_surface, (item.rect.x, item.rect.y))
        self.screen.blit(text_surface, (item.rect.x + label_surface.get_width() + 50, item.rect.y))

    def draw_ai_player_count_input(self, item: AIPlayerCount) -> None:
        label_surface = self.input_font.render(item.label, True, item.color)
        count_surface = self.input_font.render(str(item.count), True, item.color)
        add_button_surface = item.plus_button.graphic
        remove_button_surface = item.minus_button.graphic
        self.screen.blit(add_button_surface, item.plus_button.rect)
        self.screen.blit(remove_button_surface, item.minus_button.rect)
        self.screen.blit(label_surface, (item.rect.x, item.rect.y))
        self.screen.blit(count_surface, (item.rect.x + label_surface.get_width() + 10, item.rect.y))

    def draw_menu_item(self, item: MenuItem):
        match item:
            case TextButton():
                self.draw_text_button(item)
            case GraphicsButton():
                self.screen.blit(item.graphic, item.rect)
            case PlayerName():
                self.draw_player_name_input(item)
            case AIPlayerCount():
                self.draw_ai_player_count_input(item)
            case _:
                print(f"Unknown menu item type: {type(item)}")
