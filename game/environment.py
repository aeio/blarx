import gym
import pygame
from gym import spaces
import numpy as np

from game.game import Game
from game.draw import Draw
from game.settings import settings

pygame.init()


# In your Gym environment:
class CustomGameEnv(gym.Env):
    def __init__(self):
        super(CustomGameEnv, self).__init__()

        print(settings.board.font)

        # Initialize your game
        self.game = Game()
        self.draw = Draw(self.game)
        self.game.initialize_ai()

        # Assuming your action space is discrete
        self.action_space = spaces.Discrete(1)

        # Observation space includes the game board and player scores
        self.observation_space = spaces.Dict(
            {
                "board": spaces.Box(
                    low=0, high=4, shape=(self.game.board.rows, self.game.board.columns), dtype=np.int32
                ),
                "player_scores": spaces.Box(low=0, high=np.inf, shape=(self.game.players.length,), dtype=np.int32),
            }
        )

    def reset(self, **kwargs):
        self.game = Game()
        self.draw = Draw(self.game)
        self.game.initialize_ai()
        observation = self._get_observation()

        # Calculate the number of possible actions
        num_possible_actions = len(self.game.players.current_player.find_next_moves())

        self.action_space = spaces.Discrete(num_possible_actions)

        return observation

    def step(self, action):
        # Take action and return the next observation, rewards, and done flag
        self.game.players.ai_move_all()  # Execute the action
        observation = self._get_observation()

        # Calculate rewards (change in player scores)
        rewards = self.game.players.get_scores()  # Get the current scores of the players

        # Check if the game is done
        done = self.game.players.game_over  # The game is done when no more moves are available

        # Capture the Pygame screen as an image
        screen_image = pygame.surfarray.array3d(pygame.display.get_surface())
        # Transpose the image to make it suitable for rendering
        screen_image = np.transpose(screen_image, (1, 0, 2))

        return observation, rewards, done, {}, screen_image

    def render(self, mode="human"):
        if mode == "rgb_array":
            # Capture the Pygame screen as an image
            screen_image = pygame.surfarray.array3d(pygame.display.get_surface())
            # Transpose the image to make it suitable for rendering
            screen_image = np.transpose(screen_image, (1, 0, 2))
            return screen_image
        elif mode == "human":
            self.draw.draw_game()
            pass  # replace with your existing rendering code
        else:
            super(CustomGameEnv, self).render(mode=mode)  # just call the super class render function

    def _get_observation(self):
        # Helper method to get the observation
        observation = {
            "board": np.array(self.game.board.get_board_state()).flatten(),
            "player_scores": self.game.players.get_scores(),
        }
        return observation
