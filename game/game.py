import logging

import pygame

from game.board import Board
from game.player import Player, Players
from game.settings import settings

logging.basicConfig(level=logging.DEBUG, format="\x1b[0m%(levelname)s - %(message)s")


class Game:
    board: Board
    players: Players
    bg_color: tuple[int, int, int]
    running = True

    def __init__(self):
        self.bg_color = tuple(settings.board.bg_color)

    def initialize_human(self):
        self.board = Board()
        self.players = Players()
        colors = settings.board.player_colors
        for i in range(settings.players.ai_players + len(settings.players.human_names)):
            if i < len(settings.players.human_names):
                player = Player(settings.players.human_names[i], colors[i], i + 1, self.board)
                player.initialize()
                pass
            else:
                player = Player(f"AI {i+1}", colors[i], i + 1, self.board, True)
                player.randomize_name()
                player.initialize()
            logging.info(f"Player {i + 1} is {player.name}")
            self.players.add(player)

    def initialize_ai(self, count: int = 4):
        self.board = Board(self)
        self.players = Players()
        colors = settings.board.player_colors
        for i in range(count):
            player = Player(f"AI {i+1}", colors[i], i + 1, self.board, True)
            player.randomize_name()
            player.initialize()
            logging.info(f"Player {i + 1} is {player.name}")
            self.players.add(player)

    def handle_event(self, event: pygame.event.Event):
        if event.type == pygame.QUIT:
            self.running = False
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            self.left_mouse_down(event.pos)
        elif event.type == pygame.MOUSEMOTION:
            self.mouse_motion(event.pos)
        elif event.type == pygame.MOUSEWHEEL and event.y != 0:
            self.scroll(event.y)

    def left_mouse_down(self, position: tuple[int, int]):
        logging.debug(f"Left mouse down at {position}")
        self.board.get_board_state(self.players)
        if not self.players.current_player.lost:
            if self.players.current_player.grab_or_drop(position):
                if self.players.next_player():
                    self.initialize_human()

    def mouse_motion(self, position: tuple[int, int]):
        self.players.current_player.move_dragged_piece(position)

    def scroll(self, direction: int):
        self.players.current_player.rotate_dragged_piece(direction)
