import pygame
from pygame import Rect

from game.game import Game
from game.settings import settings, save_player_settings


class MenuItem:
    font: pygame.font.Font
    color: tuple[int, int, int]
    rect: Rect | None
    txt_surface: pygame.Surface
    items: list["MenuItem"]
    game: Game
    menu: "Menu"

    def __init__(
        self,
        game: Game,
        menu: "Menu",
        rect: Rect | None = None,
        color: tuple[int, int, int] | None = None,
    ):
        self.game = game
        self.menu = menu
        self.rect = rect
        if rect:
            self.rect.center = (rect.x, rect.y)
        if color:
            self.color = color
        else:
            self.color = settings.menu.color

    def handle_event(self, event):
        raise NotImplementedError


class Button(MenuItem):
    def __init__(self, game, menu, rect: Rect):
        super().__init__(game, menu, rect)

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                self.button_press()

    def button_press(self):
        raise NotImplementedError


class TextButton(Button):
    text: str

    def __init__(self, game, menu, rect: Rect, text: str = ""):
        super().__init__(game, menu, rect)
        self.text = text


class GraphicsButton(Button):
    graphic: pygame.Surface


class QuitButton(TextButton):
    def __init__(self, game, menu, rect: Rect):
        super().__init__(game, menu, rect, "quit")

    def button_press(self):
        self.game.running = False


class RestartButton(TextButton):
    def __init__(self, game, menu, rect: Rect):
        super().__init__(game, menu, rect, "restart")

    def button_press(self):
        self.game.initialize_human()
        self.menu.is_active = False


class SaveAndRestartButton(TextButton):
    def __init__(self, game, menu, rect: Rect):
        super().__init__(game, menu, rect, "restart")

    def button_press(self):
        settings.players.human_names[0] = self.menu.settings_menu.player_name.text
        settings.players.ai_players = self.menu.settings_menu.ai_players.count
        save_player_settings()
        self.game.initialize_human()
        self.menu.is_active = False


class SettingsButton(TextButton):
    def __init__(self, game, menu, rect: Rect):
        super().__init__(game, menu, rect, "settings")

    def button_press(self):
        self.menu.active_menu = self.menu.settings_menu


class CloseMenuButton(GraphicsButton):
    def __init__(self, game, menu, rect: Rect):
        super().__init__(game, menu, rect)
        # generate "X" shape for close button by drawing lines
        self.graphic = pygame.Surface((rect.w, rect.h))
        pygame.draw.line(self.graphic, settings.menu.color, (0, 0), (rect.w, rect.h), 5)
        pygame.draw.line(self.graphic, settings.menu.color, (rect.w, 0), (0, rect.h), 5)
        self.graphic.set_colorkey((0, 0, 0))

    def button_press(self):
        self.menu.is_active = False


class OpenMenuButton(GraphicsButton):
    def __init__(self, game, menu, rect: Rect):
        super().__init__(game, menu, rect)
        # generate 2 horizontal lines to open menu
        self.graphic = pygame.Surface((rect.w, rect.h))
        pygame.draw.line(self.graphic, settings.menu.color, (0, 5), (rect.w, 5), 4)
        pygame.draw.line(self.graphic, settings.menu.color, (0, rect.h / 2), (rect.w, rect.h / 2), 4)
        pygame.draw.line(self.graphic, settings.menu.color, (0, rect.h - 5), (rect.w, rect.h - 5), 4)
        self.graphic.set_colorkey((0, 0, 0))

    def button_press(self):
        self.menu.active_menu = self.menu.main_menu
        self.menu.is_active = True


class PlayerName(MenuItem):
    def __init__(self, game, menu, rect: Rect):
        super().__init__(game, menu, rect)
        self.label = "name"
        self.text = settings.players.human_names[0]
        self.active = False

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN and self.rect.collidepoint(event.pos):
            # Activate the input box when clicked
            self.active = not self.active
        elif event.type == pygame.KEYDOWN and self.active:
            if event.key == pygame.K_BACKSPACE:
                # Remove the last character
                self.text = self.text[:-1]
            else:
                # Append the pressed key to the text
                self.text += event.unicode


class AIPlayerCount(MenuItem):
    label: str
    count: int
    plus_button: "AddAiPlayerButton"
    minus_button: "RemoveAiPlayerButton"

    def __init__(self, game, menu, rect: Rect, label="AI Players:"):
        super().__init__(game, menu, rect)
        self.label = label
        self.count = settings.players.ai_players
        self.plus_button = AddAiPlayerButton(game, menu, Rect(rect.x + rect.w - 30, rect.y + 5, 20, 20))
        self.minus_button = RemoveAiPlayerButton(game, menu, Rect(rect.x + rect.w - 30, rect.y + rect.h - 35, 20, 20))

    def handle_event(self, event):
        # Handle events for the plus and minus buttons
        self.plus_button.handle_event(event)
        self.minus_button.handle_event(event)

    def increase_count(self):
        if self.count < 3:
            self.count += 1

    def decrease_count(self):
        if self.count > 0:
            self.count -= 1


class AddAiPlayerButton(GraphicsButton):
    def __init__(self, game, menu, rect: Rect):
        super().__init__(game, menu, rect)
        self.graphic = pygame.Surface((rect.w, rect.h))
        pygame.draw.line(self.graphic, settings.menu.color, (rect.w / 2, 0), (rect.w / 2, rect.h), 5)
        pygame.draw.line(self.graphic, settings.menu.color, (0, rect.h / 2), (rect.w, rect.h / 2), 5)
        self.graphic.set_colorkey((0, 0, 0))

    def button_press(self):
        self.menu.settings_menu.ai_players.increase_count()


class RemoveAiPlayerButton(GraphicsButton):
    def __init__(self, game, menu, rect: Rect):
        super().__init__(game, menu, rect)
        self.graphic = pygame.Surface((rect.w, rect.h))
        pygame.draw.line(self.graphic, settings.menu.color, (0, rect.h / 2), (rect.w, rect.h / 2), 5)
        self.graphic.set_colorkey((0, 0, 0))

    def button_press(self):
        self.menu.settings_menu.ai_players.decrease_count()


class SettingsMenu(MenuItem):
    player_name: PlayerName
    ai_players: AIPlayerCount
    restart: Button
    close_menu: Button

    def __init__(self, game, menu):
        super().__init__(game, menu)
        screen_width = settings.screen.width
        self.player_name = PlayerName(game, menu, Rect(screen_width / 2, 300, 400, 75))
        self.ai_players = AIPlayerCount(game, menu, Rect(screen_width / 2, 400, 400, 75))
        self.restart = SaveAndRestartButton(game, menu, Rect(screen_width / 2, 500, 400, 75))
        self.close_menu = CloseMenuButton(game, menu, Rect(screen_width - 50, 50, 50, 50))
        self.items = [self.player_name, self.ai_players, self.restart, self.close_menu]

    def handle_event(self, event):
        for item in self.items:
            item.handle_event(event)


class MainMenu(MenuItem):
    settings: Button
    restart: Button
    quit: Button
    close_menu: Button

    def __init__(self, game: Game, menu: "Menu"):
        super().__init__(game, menu)
        screen_width = settings.screen.width
        self.settings = SettingsButton(game, menu, Rect(screen_width / 2, 250, 400, 75))
        self.restart = RestartButton(game, menu, Rect(screen_width / 2, 375, 400, 75))
        self.quit = QuitButton(game, menu, Rect(screen_width / 2, 500, 400, 75))
        self.close_menu = CloseMenuButton(game, menu, Rect(screen_width - 50, 50, 50, 50))
        self.items = [self.settings, self.restart, self.quit, self.close_menu]

    def handle_event(self, event):
        for item in self.items:
            item.handle_event(event)


class Menu:
    is_active: bool = False
    active_menu: MenuItem
    open_menu_button: Button
    main_menu: MainMenu
    settings_menu: SettingsMenu
    game: Game

    def __init__(self, game: Game):
        self.game = game
        self.main_menu = MainMenu(game, self)
        self.settings_menu = SettingsMenu(game, self)
        self.active_menu = self.main_menu
        self.open_menu_button = OpenMenuButton(game, self, Rect(settings.screen.width - 50, 50, 30, 30))

    def handle_event(self, event):
        if event.type == pygame.QUIT:
            self.game.running = False
        else:
            if self.is_active:
                self.active_menu.handle_event(event)
            else:
                self.open_menu_button.handle_event(event)
                self.game.handle_event(event)
