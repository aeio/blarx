import logging

import numpy
import pygame


class Piece:
    shape: list[list[int]]
    name: str
    score: int = 0
    screen: pygame.Surface
    block_size: int
    position: tuple[int, int]
    tiles: list[pygame.Rect]
    dragged = False
    played = False
    mouse_offset = (0, 0)

    def __init__(
        self,
        shape: list[list[int]],
        name: str,
        block_size: int,
        position: tuple[int, int],
        color: tuple[int, int, int],
    ):
        self.shape = shape
        self.name = name
        self.block_size = block_size
        self.position = position
        self.color = color
        self.rotation = 0
        self._set_bounds()

    def _set_bounds(self) -> None:
        self.height = len(self.shape) * self.block_size
        self.width = len(self.shape[0]) * self.block_size
        self.tiles = []
        x, y = self.position
        for row in self.shape:
            x = self.position[0]
            for cell in row:
                if cell:
                    self.tiles.append(pygame.Rect(x, y, self.block_size, self.block_size))
                x += self.block_size
            y += self.block_size
        self.score = len(self.tiles)

    def grab(self, position: (int, int)) -> None:
        self.mouse_offset = tuple(numpy.subtract(self.position, position))
        logging.debug(f"Grabbed piece at {self.position} with offset {self.mouse_offset} from {position}")
        self.dragged = True

    def rotate(self) -> None:
        self.shape = list(zip(*self.shape[::-1]))
        self._set_bounds()

    def move(self, position: (int, int), offset: (int, int) = (0, 0)) -> None:
        self.position = tuple(numpy.add(position, offset))
        self._set_bounds()

    def collides_with_piece(self, piece: "Piece", inflate=(0, 0)) -> bool:
        for tile in self.tiles:
            inflated_tile = tile.inflate(inflate)
            if inflated_tile.collidelist(piece.tiles) != -1:
                return True
        return False

    def collides_with_point(self, position: tuple[int, int], inflate=(0, 0)) -> bool:
        for tile in self.tiles:
            inflated_tile = tile.inflate(inflate)
            if inflated_tile.collidepoint(position):
                return True
        return False

    def is_adjacent_to(self, piece: "Piece") -> bool:
        if piece is self:
            return False
        for my_tile in self.tiles:
            expanded_bound = my_tile.inflate(2, 2)
            for other_tile in piece.tiles:
                if expanded_bound.clip(other_tile).width > 2 or expanded_bound.clip(other_tile).height > 2:
                    return True
        return False

    def is_touching_edge(self, piece: "Piece") -> bool:
        if piece is self:
            return False
        for my_tile in self.tiles:
            expanded_bound = my_tile.inflate(2, 2)
            for other_tile in piece.tiles:
                if 2 > expanded_bound.clip(other_tile).width > 0 and 2 > expanded_bound.clip(other_tile).height > 0:
                    return True
        return False

    def get_abstract_shape(self) -> list[list[int]]:
        abstract_shape = []
        for row in self.shape:
            abstract_row = []
            for cell in row:
                abstract_row.append(1 if cell else 0)
            abstract_shape.append(abstract_row)
        return abstract_shape

    def copy(self):
        return Piece(self.shape, self.name, self.block_size, self.position, self.color)
