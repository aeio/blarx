import logging
import random
from copy import deepcopy

import pygame
import numpy
import names

from game.board import Board
from game.piece import Piece
from game.piece_shapes import piece_shapes
from game.settings import settings


class Player:
    pieces: list[Piece]
    name: str
    enemy: bool
    ai: bool
    color: (int, int, int)
    number: int
    score: int
    score_total: int
    score_position: tuple[int, int]
    dragging = False
    dragged_piece: Piece = None
    lost = False
    won = False
    board: Board
    next_moves: list[tuple[int, int]]
    next: "Player" = None

    def __init__(
        self,
        name: str,
        color: (int, int, int),
        number: int,
        board: Board,
        ai=False,
    ):
        self.bounds_y = None
        self.bounds_x = None
        self.name = name
        self.color = color
        self.number = number
        self.ai = ai
        self.score_position = (0, 0)
        self.board = board

    def randomize_name(self) -> None:
        self.name = names.get_first_name()

    def initialize(self) -> None:
        self.pieces = []
        self.next_moves = []

        self.bounds_x, self.bounds_y, self.score_position = self.calculate_bounds_and_position()

        trying = True
        while trying:
            for shape, name in piece_shapes:
                x = random.randint(int(self.bounds_x[0]), int(self.bounds_x[1]))
                y = random.randint(int(self.bounds_y[0]), int(self.bounds_y[1]))
                piece = Piece(
                    shape,
                    name,
                    settings.board.block_size,
                    (x, y),
                    self.color,
                )
                if not self.toss_piece(piece):
                    break
                self.pieces.append(piece)
            if len(self.pieces) == len(piece_shapes):
                trying = False
            else:
                self.pieces = []
                logging.debug(f"piece toss failed, trying again for {self.name}")

        self.score_total = sum([piece.score for piece in self.pieces])
        self.score = 0

    def calculate_bounds_and_position(self):
        screen_width = settings.screen.width
        screen_height = settings.screen.height
        block_size = settings.board.block_size
        columns = settings.board.columns
        rows = settings.board.rows

        bounds_x = None
        bounds_y = None
        score_position = None

        if self.number == 1:
            bounds_x = (20, (screen_width / 2) - 100)
            bounds_y = ((screen_height / 2) + block_size * (rows / 2) + 20, screen_height - 100)
            score_position = (20, (screen_height / 2) + 20)
        elif self.number == 2:
            bounds_x = (20, (screen_width / 2) - 100)
            bounds_y = (20, (screen_height / 2) - (block_size * (rows / 2) + 100))
            score_position = (20, (screen_height / 2) - 68)
        elif self.number == 3:
            bounds_x = ((screen_width / 2) + 20, screen_width - 100)
            bounds_y = (20, (screen_height / 2) - block_size * (rows / 2) - 100)
            score_position = ((screen_width / 2) + (columns * block_size / 2) + 20, (screen_height / 2) - 68)
        elif self.number == 4:
            bounds_x = ((screen_width / 2) + 20, screen_width - 100)
            bounds_y = ((screen_height / 2) + block_size * (rows / 2) + 20, screen_height - 100)
            score_position = ((screen_width / 2) + (columns * block_size / 2) + 20, (screen_height / 2) + 20)

        return bounds_x, bounds_y, score_position

    def toss_piece(self, piece: Piece):
        bounds_x_min, bounds_x_max = self.bounds_x
        bounds_y_min, bounds_y_max = self.bounds_y

        for _ in range(1000):
            x = random.randint(int(bounds_x_min), int(bounds_x_max))
            y = random.randint(int(bounds_y_min), int(bounds_y_max))

            piece.move((x, y))

            if not self.piece_collides_with_pieces(piece, inflate=(5, 5)):
                piece.dragged = False
                return True

            for _ in range(4):
                piece.rotate()
                if not self.piece_collides_with_pieces(piece, inflate=(5, 5)):
                    piece.dragged = False
                    return True

        return False

    def piece_collides_with_pieces(self, piece: Piece, inflate=(0, 0)):
        for player_piece in self.pieces:
            if player_piece.collides_with_piece(piece, inflate) and piece is not player_piece:
                return player_piece
        return False

    def point_collides_with_pieces(self, position: (int, int)) -> Piece | None:
        for player_piece in self.pieces:
            if player_piece.collides_with_point(position):
                return player_piece
        return None

    def move_dragged_piece(self, position: tuple[int, int]) -> None:
        if not self.dragging:
            return

        if not all(
            self.board.bounds.contains(bound) for bound in self.dragged_piece.tiles
        ) or not self.board.bounds.contains(pygame.Rect(position, (1, 1))):
            self.dragged_piece.move(position, self.dragged_piece.mouse_offset)
            return
        else:
            for row in self.board.snap_points:
                for snap_point in row:
                    diff_x, diff_y = tuple(
                        numpy.subtract(numpy.add(position, self.dragged_piece.mouse_offset), snap_point)
                    )
                    half_block_size = self.board.block_size / 2

                    if (
                        -half_block_size < diff_x < half_block_size
                        and -half_block_size < diff_y < half_block_size
                        and all(self.board.bounds.contains(bound) for bound in self.dragged_piece.tiles)
                    ):
                        # player position close to snap point and piece is on the board
                        self.dragged_piece.move(snap_point)
                        return

    def rotate_dragged_piece(self, y: int) -> None:
        if not self.dragging:
            return

        for player_piece in self.pieces:
            if not player_piece.dragged:
                continue

            if y > 0:
                player_piece.rotate()
            else:
                player_piece.rotate()

    def drop_dragged_piece(self) -> bool:
        self.dragging = False
        self.dragged_piece.dragged = False
        if not self.validate_move(self.dragged_piece):
            self.toss_piece(self.dragged_piece)
            logging.info(f"{self.name} dropped {self.dragged_piece.name} in invalid position")
            return False
        self.score += self.dragged_piece.score
        self.dragged_piece.played = True
        return True

    def grab_piece(self, piece: Piece, position: (int, int)):
        piece.grab(position)
        self.dragging = True
        self.dragged_piece = piece

    def grab_or_drop(self, position: (int, int)):
        if self.dragging:
            logging.info(f"{self.name} dropped {self.dragged_piece.name}")
            return self.drop_dragged_piece()

        piece = self.point_collides_with_pieces(position)
        if piece and not piece.played:
            self.grab_piece(piece, position)
            logging.info(f"{self.name} grabbed {piece.name}")
            return False

    def validate_move(self, piece: Piece):
        for bound in piece.tiles:
            if not self.board.bounds.contains(bound):
                self.toss_piece(piece)
                return False

        # check if piece is touching any other piece on the board
        next_player = self.next
        while next_player != self:
            for player_piece in next_player.pieces:
                if piece.collides_with_piece(player_piece):
                    return False
            next_player = next_player.next

        if self.score == 0:
            # check if first piece is in a corner
            if (
                piece.collides_with_point(self.board.bounds.topleft, inflate=(2, 2))
                or piece.collides_with_point(self.board.bounds.topright, inflate=(2, 2))
                or piece.collides_with_point(self.board.bounds.bottomleft, inflate=(2, 2))
                or piece.collides_with_point(self.board.bounds.bottomright, inflate=(2, 2))
            ):
                return True
            else:
                return False

        # check if piece is not directly next to any of the player's pieces
        for player_piece in self.pieces:
            if player_piece.is_adjacent_to(piece):
                return False

        # check if piece is touching edge of one of the player's pieces
        for player_piece in self.pieces:
            if player_piece.is_touching_edge(piece):
                return True

        return False

    def make_random_move(self) -> int:
        """Returns score of the move, 0 if the player can't make a valid move"""
        random.shuffle(self.pieces)
        snap_points = self.board.snap_points

        if self.score == 0:
            for piece in self.pieces:
                if not piece.played:
                    rows = len(self.board.snap_points)
                    cols = len(self.board.snap_points[0])

                    for row_index in [0, rows - 1]:
                        for col_index in [0, cols - 1]:
                            snap_point = self.board.snap_points[row_index][col_index]

                            rotations = 0
                            while rotations < 4:
                                if self.try_move(snap_point, piece):
                                    piece.move(snap_point)
                                    piece.played = True
                                    return piece.score
                                piece.rotate()
                                rotations += 1

            return 0

        for piece in self.pieces:
            if not piece.played:
                for snap_point in random.sample(
                    [sp for row in snap_points for sp in row], len(snap_points) * len(snap_points[0])
                ):
                    rotations = 0
                    while rotations < 4:
                        if self.try_move(snap_point, piece):
                            piece.move(snap_point)
                            piece.played = True
                            return piece.score
                        piece.rotate()
                        rotations += 1

        return 0

    def find_next_moves(self) -> list[tuple[int, int]]:
        """Returns a list of valid moves"""
        moves = []
        snap_points = self.board.snap_points
        snap_points_rows = len(snap_points)
        snap_points_cols = len(snap_points[0])

        for piece in self.pieces:
            if not piece.played:
                for row in range(snap_points_rows):
                    for col in range(snap_points_cols):
                        for rotations in range(4):
                            if self.try_move(snap_points[row][col], piece):
                                moves.append((snap_points[row][col], piece))
                            piece.rotate()
        return moves

    def still_alive(self) -> bool:
        """Checks if the player can still make a valid move"""
        snap_points = self.board.snap_points
        snap_points_rows = len(snap_points)
        snap_points_cols = len(snap_points[0])

        for piece in self.pieces:
            if not piece.played:
                # Precompute all 4 rotations of the piece
                rotated_pieces = [deepcopy(piece)]
                for _ in range(3):
                    rotated_piece = deepcopy(rotated_pieces[-1])
                    rotated_piece.rotate()
                    rotated_pieces.append(rotated_piece)

                # Check all snap points for any valid move with any rotation
                for row in range(snap_points_rows):
                    for col in range(snap_points_cols):
                        position = snap_points[row][col]
                        if any(self.try_move(position, rotated_piece) for rotated_piece in rotated_pieces):
                            print(f"{self.name} can still move {piece.name} to {position}")
                            return True
        return False

    def try_move(self, position: tuple[int, int], piece: Piece) -> bool:
        test_piece = deepcopy(piece)
        test_piece.move(position)
        if self.validate_move(test_piece):
            return True
        return False


class Players:
    current_player: Player
    game_over: bool = False
    length: int
    active_players: list[Player]

    def __init__(self):
        self.length = 0

    def add(self, player: Player):
        if self.length == 0:
            self.current_player = player
            self.current_player.next = player
        else:
            player.next = self.current_player.next
            self.current_player.next = player
        self.length += 1

    def get_list(self) -> list[Player]:
        players = []
        player = self.current_player
        for i in range(self.length):
            players.append(player)
            player = player.next
        return players

    def get_current(self):
        return self.current_player

    def get_winners(self) -> list[Player] | None:
        player = self.current_player
        still_active = []
        for i in range(self.length):
            if not player.lost:
                still_active.append(player)
            player = player.next

        if len(still_active) > 0:
            self.active_players = still_active
            return None
        else:
            self.game_over = True
            for player in self.active_players:
                player.lost = False
            logging.info(
                f"Game Over! No more moves available. Winners: {[player.name for player in self.active_players]}"
            )
            return self.active_players

    def get_scores(self) -> list[int]:
        scores = []
        player = self.current_player
        for i in range(self.length):
            scores.append(player.score)
            player = player.next
        return scores

    def next_player(self):
        # check if next player is still alive
        if not self.current_player.still_alive():
            self.current_player.lost = True
        else:
            logging.info(f"Player {self.current_player.name} is still alive")

        # check if game is over
        winners = self.get_winners()
        if winners is not None:
            return

        # move to next player
        self.current_player = self.current_player.next

        while self.current_player.ai:
            if not self.current_player.lost:
                move_score = self.current_player.make_random_move()
                if move_score:
                    self.current_player.score += move_score
                else:
                    self.current_player.lost = True
            self.current_player = self.current_player.next

        logging.info(f"Player {self.current_player.name}'s turn")

    def ai_move_all(self) -> bool:
        player = self.current_player
        current_winners = []
        for i in range(self.length):
            if player.ai and not player.lost:
                print(player.find_next_moves())
                if player.make_random_move():
                    player.score += 1
                    current_winners.append(player)
                else:
                    player.lost = True
            player = player.next

        if len(current_winners) > 0:
            self.active_players = current_winners
        else:
            self.game_over = True
            logging.info(
                f"Game Over! No more moves available. Winners: {[player.name for player in self.active_players]}"
            )
            return False

        logging.info("All AI players have moved")

        return True