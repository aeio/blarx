import json

from pydantic import BaseModel

GAME_SETTINGS = "settings/game.json"
PLAYER_SETTINGS = "settings/players.json"


class ScreenSettings(BaseModel):
    height: int
    width: int


class BoardSettings(BaseModel):
    block_size: int
    rows: int
    columns: int
    bg_color: list[int]
    board_color: list[int]
    player_colors: list[list[int]]
    font: str


class MenuSettings(BaseModel):
    color: list[int]
    font: str


class PlayerSettings(BaseModel):
    human_names: list
    ai_players: int


class GameSettings(BaseModel):
    board: BoardSettings
    screen: ScreenSettings
    menu: MenuSettings
    players: PlayerSettings
    debug: bool


def load_settings():
    f = open(GAME_SETTINGS)
    game_settings = json.load(f)
    f = open(PLAYER_SETTINGS)
    game_settings["players"] = json.load(f)
    return game_settings


settings = GameSettings(**load_settings())


def save_player_settings():
    f = open(PLAYER_SETTINGS, "w")
    return f.write(json.dumps(settings.players.model_dump(), indent=2))
